import serial
from time import sleep

# Set the port to match that of the radio: 
port = "/dev/ttyUSB0"
#port = "COM1"
# Set the radio's CI-V address here:
rig_civAddress = b"\xA4"
# Type the command you wish to test here: 
command = b"\x1D\x19"

this_civAddress = b"\xE0"
pre = b"\xFE\xFE" + rig_civAddress + this_civAddress
ending = b"\xFD"

print("Opening port " + port)
ser = serial.Serial(port, 115200, timeout=1)


print("pre: " + pre.hex());
print("command: " + command.hex())
print("ending: " + ending.hex())

print("Sending command.")
print(pre.hex() + command.hex() + ending.hex())

ser.write(pre + command + ending)

print("Reading from port...")
while(1):
    data = ser.read(size=64)
    if len(data) > 0 :
        print("Data read: " + data.hex())
        print("Press Control-C to exit.")
        print("Reading from port...")
    sleep(0.1)



